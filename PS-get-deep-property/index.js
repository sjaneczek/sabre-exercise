
const testObject = {
  "send": {
    "higher": {
      "further": {
        "try": "plural",
        "arrive": "state",
        "red": {
          "interior": "oxygen",
          "cross": 'Hello world',
          "hair": "subject",
          "seb": ['1', '2', '3']
        }
      },
      "flies": "grade",
      "strong": "shade"
    },
    "leg": "follow",
    "attention": "watch"
  },
  "color": "gasoline",
  "nine": "through"
}

const checkProperty = (object, key) => {
  if (object.hasOwnProperty(key)) {
    return object[key];
  }
  return undefined;
}

const propertier = (data, arr, fallback) => {
  if (typeof data !== "object" || data.length) {
    console.warn('Please provide valid object');
    return;
  }

  const value = arr.reduce(checkProperty, data);

  if (value !== undefined) {
    return value;
  }
  
  return fallback;
}

const validTest = propertier(testObject, ['send', 'higher', 'further', 'red', 'cross'], 'Seb');
const invalidTest = propertier(['send', 'higher', 'further', 'red']);
const testWithDefault = propertier(testObject, ['send', 'higher', 'further', 'red', 1], 'my default value');
const testWithNoDefault = propertier(testObject, ['send', 'higher', 'further', 'red', 1]);


console.log('Valid test: ', validTest);
console.log('Invalid test: ', invalidTest);
console.log('Default value test: ', testWithDefault);
console.log('No default value test: ', testWithNoDefault);