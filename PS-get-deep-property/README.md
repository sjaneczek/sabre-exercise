# PS-get-deep-property exercise

## Instructions

- Using vanilla javascript, create a function that gets the value of a property at a given path
- Example:
  - If given the object: {person: {name: {first: 'FirstName', middleInitial: 'I', lastName: 'LastName''}}}
  - And given the path: 'person.name.lastName'
  - The output would be: 'LastName'
  * Note this is just a simple example. Your function should work with any object that includes any value.
- After you complete the exercise, provide any notes on your code below such as how to run your example

## Candidate Notes:

- To run test cases all you need to do is open index.html file and open the console all possible test cases should console log with desired result. 

- Inside index.js you can change content of testObject file or values passed to the function which looks into the object and gets the correct value if exists. 

- Function `propertier(object, array, fallback)` is the main function to get the nested value, other function are the helpers. Properties accepted are: 
  - 1. Object - the actual object (data)
  - 2. Array - list of nested keys starting with the first one
  - 3. Fallback (optional) - fallback value if key does not exists on the object. 