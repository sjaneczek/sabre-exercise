
export interface Feed {
  id: number,
  user: string,
  value: string,
  timestamp: string,
  timeZoneOffset: string,
  likes: number;
}

export interface FeedData {
  feed: Feed[];
}