import React, { FunctionComponent } from 'react';
import * as S from './styles';
import { ColorVariants } from '../../types';

interface ButtonProps {
  children: string;
  variant?: ColorVariants;
  btnHandler?: () => void;
}

const Button: FunctionComponent<ButtonProps> = ({
  children,
  variant,
  btnHandler,
}) => <S.ButtonWrapper onClick={btnHandler} color={variant}>{children}</S.ButtonWrapper>;

export default Button;