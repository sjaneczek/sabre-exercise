import styled, { textStyles } from '../../styles/theme';
import { ColorVariants } from '../../types';

interface ButtonWrapperProps {
  color?: ColorVariants;
}

export const ButtonWrapper = styled.button<ButtonWrapperProps>`
  ${textStyles};
  background-color: ${props => props.color ? props.theme[props.color] : props.theme.primary};
  color: ${props => props.color === 'warning' || props.color === 'light' ? 
  props.theme.dark : props.theme.white};

  border: 0;
  border-radius: 3px;
  padding: 6px 10px;
  cursor: pointer;

  &:focus {
    outline: none;
  }
`;