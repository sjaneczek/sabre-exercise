import React, { FunctionComponent } from 'react';
import moment from 'moment';
import FeedItem from '../FeedItem';
import * as S from './styles';
import { Feed } from '../../interfaces/feed';

interface FeedListProps {
  feeds: Feed[];
  likeHandler: (id: number) => void;
}

const FeedList: FunctionComponent<FeedListProps> = ({
  feeds,
  likeHandler,
}) => {
  const feedItems = feeds.map(feed => {
    const feedDate = moment(parseInt(feed.timestamp)).format('YYYY-MM-DD HH:mm');
    return (
      <FeedItem
        {...feed}
        key={feed.id}
        timestamp={feedDate}
        likeHandler={likeHandler}
      />
    )
  })
  return (
    <S.FeedListWrapper>
      {feedItems}
    </S.FeedListWrapper>
  );
}

export default FeedList;