import styled from '../../styles/theme';

export const FeedListWrapper = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`;
