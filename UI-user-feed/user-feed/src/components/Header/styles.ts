import styled, { textStyles } from '../../styles/theme';

export const HeaderWrapper = styled.header`
  display: flex;
  align-items: center;
  margin-bottom: 24px;
`;

export const HeaderText = styled.h1`
  ${textStyles};

  margin: 0 10px 0 0;
  font-weight: 400;
  font-size: 1.5rem;
`;