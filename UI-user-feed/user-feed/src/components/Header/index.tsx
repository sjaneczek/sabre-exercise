import React, { FunctionComponent } from 'react';
import Badge from '../Badge';
import * as S from './styles';

interface HeaderProps {
  children: string;
  itemsNo?: number;
}

const Header: FunctionComponent<HeaderProps> = ({
  children,
  itemsNo,
}) => (
  <S.HeaderWrapper>
    <S.HeaderText>{children}</S.HeaderText>
    {itemsNo && <Badge itemsNo={itemsNo}>{`${itemsNo > 1 ? 'feeds' : 'feed'}`}</Badge>}
  </S.HeaderWrapper>
);

export default Header;