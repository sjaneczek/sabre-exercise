import React, { FunctionComponent } from 'react';
import Button from '../Button';
import { Feed } from '../../interfaces/feed';
import * as S from './styles';

interface FeedItemProps extends Feed {
  likeHandler: (id: number) => void;
}

const FeedItem: FunctionComponent<FeedItemProps> = ({
  id,
  user,
  value,
  timestamp,
  likes,
  likeHandler
}) => (
  <S.FeedItemWrapper>
    <S.FeedItemHeader>
      <S.FeedItemUser>{user}</S.FeedItemUser>
      <S.FeedItemDate>{timestamp}</S.FeedItemDate>
    </S.FeedItemHeader>
    <S.FeedItemText>{value}</S.FeedItemText>
    <S.FeedItemLikeWrapper>
      <Button btnHandler={() => likeHandler(id)} variant="like">
        {`${likes} ${likes > 1 ? 'likes' : 'like'}`}
      </Button>
    </S.FeedItemLikeWrapper>
  </S.FeedItemWrapper>
);

export default FeedItem;