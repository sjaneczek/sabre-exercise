import styled, { textStyles } from '../../styles/theme';

export const FeedItemWrapper = styled.li`
  ${textStyles};
  padding: 24px 0;
  border-top: 1px solid ${props => props.theme.highlight};

  &:last-child {
    border-bottom: 1px solid ${props => props.theme.highlight};
  }
`;

export const FeedItemHeader = styled.div`
  display: flex;
  align-items: center;
`;

export const FeedItemUser = styled.h2`
  font-size: 1rem;
  margin: 0 10px 0 0;
`;

export const FeedItemDate = styled.span`
  font-size: 0.7rem;
  color: ${props => props.theme.light};
`;

export const FeedItemText = styled.p`
  font-size: 0.9rem;
`;

export const FeedItemLikeWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;
