import styled, { textStyles } from '../../styles/theme';

interface FeedFormInputProps {
  error?: boolean;
}

export const FeedFormWrapper = styled.div`
  margin-bottom: 32px;
`;

export const FeedForm = styled.form`
  display: flex;
  align-items: center;
`;

export const FeedFormInput = styled.input<FeedFormInputProps>`
  flex-grow: 1;
  margin-right: 10px;
  border-radius: 3px;
  border: 1px solid ${props => props.error ?
    props.theme.danger : props.theme.highlight};
  padding: 5px;

  &:focus {
    border: 1px solid ${props => props.error ?
      props.theme.danger : props.theme.primary};
    outline: none;
  }
`;

export const FeedFormError = styled.p`
  ${textStyles};
  font-size: 0.7rem;
  color: ${props => props.theme.danger};
`;