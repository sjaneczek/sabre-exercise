import React, { FunctionComponent, useState, ChangeEvent, FormEvent } from 'react';
import Button from '../Button';
import * as S from './styles';

interface FeedFormProps {
  feedHandler: (feed: any) => void;
}

const FeedForm: FunctionComponent<FeedFormProps> = ({
  feedHandler,
}) => {
  const [feedValue, setFeedValue] = useState<string>('');
  const [error, setError] = useState(false);

  const onChangeText = (e: ChangeEvent<HTMLInputElement>) => {
    setError(!e?.target?.value);
    setFeedValue(e.target.value);
  }

  const onFormSubmit = (e: FormEvent) => {
    e.preventDefault();
    setError(!feedValue);

    if(feedValue) {
      feedHandler(feedValue);
      setFeedValue('');
    }
  }
  return (
    <S.FeedFormWrapper>
      <S.FeedForm onSubmit={onFormSubmit}>
        <S.FeedFormInput
          onChange={onChangeText}
          onBlur={() => setError(false)}
          value={feedValue}
          error={error}
        />
        <Button>Add feed</Button>
      </S.FeedForm>
      {error && <S.FeedFormError>Field cannot be empty. Please enter the value.</S.FeedFormError>}
    </S.FeedFormWrapper>
  );
}

export default FeedForm;
