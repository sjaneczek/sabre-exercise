import styled, { textStyles } from '../../styles/theme';
import { ColorVariants } from '../../types';

interface BadgeWrapperProps {
  color?: ColorVariants;
}

export const BadgeWrapper = styled.span<BadgeWrapperProps>`
  ${textStyles};
  background-color: ${props => props.color ? props.theme[props.color] : props.theme.primary};
  color: ${props => props.color === 'warning' || props.color === 'light' ? 
  props.theme.dark : props.theme.white};
  font-size: 0.7rem;
  padding: 6px;
  border-radius: 3px;
`;

export const BadgeItemNo = styled.span`
  margin-right: 5px;
  background-color: ${props => props.theme.white};
  color: ${props => props.theme.secondary};
  font-weight: 600;
  padding: 1px 2px;
  border-radius: 3px;
`;