import React, { FunctionComponent } from 'react';
import * as S from './styles';
import { ColorVariants } from '../../types';

interface BadgeProps {
  children: string;
  itemsNo?: number;
  variant?: ColorVariants;
}

const Badge: FunctionComponent<BadgeProps> = ({
  children,
  itemsNo,
  variant,
}) => (
  <S.BadgeWrapper color={variant}>
    {itemsNo && <S.BadgeItemNo>{itemsNo}</S.BadgeItemNo>}
    {children}
  </S.BadgeWrapper>
);

export default Badge;