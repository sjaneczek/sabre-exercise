import styled, { ThemeProvider, css } from 'styled-components';

const theme = {
  white: '#fff',
  black: '#212529',
  primary: '#17a2b8',
  secondary: '#6c757d',
  success: '#28a745',
  danger: '#dc3545',
  warning: '#ffc107',
  light: '#9BA8B2',
  highlight: '#ced4da',
  dark: '#343a40',
  like: '#007bff',
}

const textStyles = css`
  font-family: sans-serif;
  color: #6c757d;
`;

export type Theme = typeof theme;
export { css, ThemeProvider, theme, textStyles };
export default styled; 