import React, { FunctionComponent, useState } from 'react';
import moment from 'moment';
import Header from '../components/Header';
import FeedForm from '../components/FeedForm';
import FeedList from '../components/FeedList';
import feedsData from '../data/data.json';
import { Feed } from '../interfaces/feed';
import * as S from './styles';

const App: FunctionComponent = () => {
  const [feeds, setFeed] = useState<Feed[]>(feedsData.feed);

  const feedHandler = (value: string) => {
    const tweetDate = moment.utc(new Date());
    const newTweet = {
      id: feeds.length + 1,
      user: 'Sebastian',
      timestamp: `${tweetDate.utc().format("x")}`,
      timeZoneOffset: `${tweetDate.utcOffset()}`,
      likes: 0,
      value,
    }
    setFeed([
      newTweet,
      ...feeds,
    ]);
  }

  const likeHandler = (id: number) => {
    const likedIndex = feeds.findIndex(feed => feed.id === id);
    if (likedIndex !== -1) {
      const updatedFeed = {...feeds[likedIndex], likes: feeds[likedIndex].likes + 1};
      const updatedListFeed = Object.assign([], feeds, {[likedIndex]: updatedFeed});
      setFeed(updatedListFeed);
    }
  }

  return (
    <S.AppWrapper>
      <Header itemsNo={feeds.length}>User feeds</Header>
      <FeedForm feedHandler={feedHandler} />
      <FeedList feeds={feeds} likeHandler={likeHandler} />
    </S.AppWrapper>
  )
};

export default App;