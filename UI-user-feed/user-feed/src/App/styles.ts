import styled from '../styles/theme';

export const AppWrapper = styled.div`
  margin: 20px;
  overflow: scroll;
`;